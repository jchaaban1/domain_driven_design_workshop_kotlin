package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest

import com.jchaaban.domain_driven_design_workshop_kotlin.application.Raum.RaumAnlage
import com.jchaaban.domain_driven_design_workshop_kotlin.application.Raum.PersonZumRuamHinzufügung
import com.jchaaban.domain_driven_design_workshop_kotlin.application.common.RaumAbfrage
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto.RaumAbfrageDto
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto.RaumAnlageDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

@RestController
@RequestMapping("/api")
class RaumController(
    private val raumAnlage: RaumAnlage,
    private val raumAbfrage: RaumAbfrage,
    private val personZumRuamHinzufügung: PersonZumRuamHinzufügung
) {

    // TODO [REST] optimalerweise gibt es das Header-Attribute Location
    @PostMapping("/room")
    fun hinzufügen(@RequestBody raumAnlageDto: RaumAnlageDto): ResponseEntity<out Any> {
        val raumAnlageResult = raumAnlage.legeAn(raumname = raumAnlageDto.raumname, raumnummer = raumAnlageDto.raumnummer)
        return raumAnlageResult.wrapInResponse()
    }

    @GetMapping("/room/{id}")
    fun abfragen(@PathVariable id: String): ResponseEntity<out Any> {
        return raumAbfrage.frageAb(Raum.Id(UUID.fromString(id))).wrapInResponse()
    }

    @PutMapping("/room/{raumId}/person/{personId}")
    fun fügepersonZumRaumHinzu(@PathVariable raumId: String, @PathVariable personId: String): ResponseEntity<out Any> {
        return personZumRuamHinzufügung
            .fügePersonZumRaumHinzu(raumId = Raum.Id(UUID.fromString(raumId)), personId = Person.Id(UUID.fromString(personId)))
            .wrapInResponse()
    }

    private fun RaumAnlage.Result.wrapInResponse(): ResponseEntity<out Any> = when (this) {
        is RaumAnlage.RaumAngelegt -> ResponseEntity.ok(this.raum.mapToDto())
        is RaumAnlage.RaumExistiert -> ResponseEntity.badRequest()
            .body("Der Raum mit dem Nummer: ${this.raumnummer.value} existiert schon")

        is RaumAnlage.RaumInvalidArguments -> ResponseEntity.badRequest().body(this.message)
    }

    private fun Raum.mapToDto(): RaumAbfrageDto = RaumAbfrageDto(
        id = this.id.value.toString(),
        raumnummer = this.raumnummer.value,
        raumname = this.raumname.value,
        personen = emptyList()
    )

    private fun RaumAbfrage.Result.wrapInResponse(): ResponseEntity<out Any> = when (this) {
        is RaumAbfrage.BesetzterRaum -> ResponseEntity.ok(this.mapToDto())
        is RaumAbfrage.RaumExistiertNicht -> ResponseEntity.notFound().build()
    }

    private fun RaumAbfrage.BesetzterRaum.mapToDto() = RaumAbfrageDto(
        id = this.raum.id.value.toString(),
        raumnummer = this.raum.raumnummer.value,
        raumname = this.raum.raumname.value,
        personen = this.personen.map { it.kurzeSchreibweise() }.toList()
    )

    private fun PersonZumRuamHinzufügung.Result.wrapInResponse(): ResponseEntity<out Any> =
        when (this) {
            is PersonZumRuamHinzufügung.PersonZumRaumHinzugefügt -> ResponseEntity
                .ok("Die Person wurde erfolgreich zum Raum hinzugefügt")

            is PersonZumRuamHinzufügung.PersonExistiertNicht -> ResponseEntity.badRequest()
                .body("Die Person, die Sie zum Raum hinzufügen wollen existiert nicht")

            is PersonZumRuamHinzufügung.RaumExistiertNicht -> ResponseEntity.badRequest()
                .body("Der Raum existiert nicht")

            is PersonZumRuamHinzufügung.PersonExistiertImRaum -> ResponseEntity.badRequest()
                .body("Die Person existiert schon im Raum")
        }


}








