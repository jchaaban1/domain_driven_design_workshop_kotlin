package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.repository

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.RaumRepository
import org.springframework.stereotype.Repository

@Repository
class RaumRepositoryImpl : RaumRepository {

    private val raeume = mutableMapOf<Raum.Raumnummer, Raum>()
    override fun legeAn(raum: Raum) {
        raeume[raum.raumnummer] = raum
    }

    override fun lade(raumId: Raum.Id) : Raum? {
        return raeume.values.find { it.id.value == raumId.value }
    }

    override fun existiert(raumnummer: Raum.Raumnummer): Boolean {
        return raeume.containsKey(raumnummer)
    }

    override fun aktualisiere(raum: Raum) {
        raeume[raum.raumnummer] = raum
    }

    val size: Int
        get() = raeume.size
}