package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.repository

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.RaumEventRepository
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.PersonWurdeRaumZugeordnetEvent
import org.springframework.stereotype.Repository

@Repository
class PersonHinzugefügtEventRepositoryImpl(
    private val events: MutableList<PersonWurdeRaumZugeordnetEvent> = mutableListOf()
) : RaumEventRepository {
    override fun werfe(event: PersonWurdeRaumZugeordnetEvent) {
        events.add(event)
    }

    public fun size() = events.size
}