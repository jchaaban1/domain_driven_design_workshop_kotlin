package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto

import com.fasterxml.jackson.annotation.JsonProperty

class RaumAnlageDto (
    @JsonProperty(required = true) val raumnummer: String,
    @JsonProperty(required = true) val raumname: String,
)