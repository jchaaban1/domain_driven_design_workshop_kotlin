package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto

import com.fasterxml.jackson.annotation.JsonProperty

class RaumAbfrageDto (
    @JsonProperty(required = true) val id: String,
    @JsonProperty(required = true) val raumnummer: String,
    @JsonProperty(required = true) val raumname: String,
    @JsonProperty(required = true) val personen: List<String>,
)