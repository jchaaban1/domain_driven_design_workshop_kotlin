package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Person.rest

import com.jchaaban.domain_driven_design_workshop_kotlin.application.Person.PersonAnlage
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto.PersonAbfrageDto
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto.PersonAnlageDto
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/api")
class PersonController(private val personAnlage: PersonAnlage) {

    // TODO [REST] optimalerweise gibt es das Header-Attribute Location
    @PostMapping("/person")
    fun hinzufügen(@RequestBody personAnlageDto: PersonAnlageDto): ResponseEntity<out Any> {
        val personAnlageResult = personAnlage.legeAn(
            vorname = personAnlageDto.vorname,
            nachname = personAnlageDto.nachname,
            namenzusatz = personAnlageDto.namenszusatz,
            ldapBenutzername = personAnlageDto.ldapBenutzername
        )
        return personAnlageResult.wrapInResponse()
    }



    private fun PersonAnlage.Result.wrapInResponse(): ResponseEntity<out Any> = when (this) {
        is PersonAnlage.PersonAngelegt -> ResponseEntity.ok(this.person.mapToDto())
        is PersonAnlage.PersonExistiert -> ResponseEntity.badRequest()
            .body("Der Person mit dem ldapBenutzername: ${this.ldapBenutzername.value} existiert schon")

        is PersonAnlage.PersonInvalidArguments -> ResponseEntity.badRequest().body(this.message)
    }

    private fun Person.mapToDto(): PersonAbfrageDto = PersonAbfrageDto(
        id = this.id.value.toString(),
        vorname = this.vorname.value,
        nachname = this.nachname.value,
        namenszusatz = this.namenszusatz?.name?.lowercase(),
        ldapBenutzername = this.ldapBenutzername.value
    )
}