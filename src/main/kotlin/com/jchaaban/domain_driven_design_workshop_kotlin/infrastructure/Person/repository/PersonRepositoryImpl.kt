package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Person.repository

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.PersonRepository
import org.springframework.stereotype.Repository

@Repository
class PersonRepositoryImpl : PersonRepository {

    private val personen = mutableMapOf<Person.Id, Person>()
    override fun legeAn(person: Person) {
        personen[person.id] = person
    }

    override fun existiert(personLdapBenutzername: Person.LdapBenutzername) =
        personen.values.any { it.ldapBenutzername == personLdapBenutzername }

    override fun existiert(personId: Person.Id) = personen[personId] != null

    override fun holePersonenMitIds(personenIds: List<Person.Id>) =
        personen.values.filter {
            person -> personenIds.any {
                personenIds ->
                person.id == personenIds
            }
        }

    fun size() = personen.size
}