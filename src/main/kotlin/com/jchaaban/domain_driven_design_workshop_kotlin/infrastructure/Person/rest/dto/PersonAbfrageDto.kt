package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto

import com.fasterxml.jackson.annotation.JsonProperty

class PersonAbfrageDto (
    @JsonProperty(required = true)
    val id: String,
    @JsonProperty(required = true)
    val vorname: String,
    @JsonProperty(required = true)
    val nachname: String,
    @JsonProperty(required = false)
    val namenszusatz: String?,
    @JsonProperty(required = true)
    val ldapBenutzername: String
)