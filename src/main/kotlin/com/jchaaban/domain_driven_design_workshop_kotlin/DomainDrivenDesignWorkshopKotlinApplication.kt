package com.jchaaban.domain_driven_design_workshop_kotlin

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class DomainDrivenDesignWorkshopKotlinApplication

fun main(args: Array<String>) {
	runApplication<DomainDrivenDesignWorkshopKotlinApplication>(*args)
}
