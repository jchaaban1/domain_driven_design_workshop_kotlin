package com.jchaaban.domain_driven_design_workshop_kotlin.application.Person

import com.jchaaban.domain_driven_design_workshop_kotlin.common.UseCase
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.PersonRepository

@UseCase
class PersonAnlage(private val personRepository: PersonRepository) {

    // TODO [DDD] Du solltest im Service statt der Strings direkt die Value-Objects nutzen
    fun legeAn(
        vorname: String,
        nachname: String,
        namenzusatz: String?,
        ldapBenutzername: String,
    ): Result {
        val personErzeugungErgebnis = erzeugePerson(
            vorname = vorname,
            nachname = nachname,
            namenszusatz = namenzusatz,
            ldapBenutzername = ldapBenutzername
        )

        return when (personErzeugungErgebnis) {
            is Person.InvalidArguments -> PersonInvalidArguments(personErzeugungErgebnis.message)
            is Person.Created -> speicherePerson(person = personErzeugungErgebnis.person)
        }
    }

    private fun erzeugePerson(
        vorname: String,
        nachname: String,
        namenszusatz: String?,
        ldapBenutzername: String,
    ): Person.Result =
        Person(
            vorname = vorname,
            nachname = nachname,
            namenzusatz = namenszusatz,
            ldapBenutzername = ldapBenutzername
        )

    private fun speicherePerson(person: Person): Result {
        val personLdapBenutzername = person.ldapBenutzername
        if (personRepository.existiert(personLdapBenutzername)) {
            return PersonExistiert(personLdapBenutzername)
        }
        personRepository.legeAn(person)
        return PersonAngelegt(person)
    }

    sealed class Result
    class PersonAngelegt(val person: Person) : Result()
    class PersonExistiert(val ldapBenutzername: Person.LdapBenutzername) : Result()
    class PersonInvalidArguments(val message: String) : Result()

}



