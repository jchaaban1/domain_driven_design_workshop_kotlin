package com.jchaaban.domain_driven_design_workshop_kotlin.application.common

import com.jchaaban.domain_driven_design_workshop_kotlin.common.UseCase
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.PersonRepository
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.RaumRepository

@UseCase
class RaumAbfrage(private val raumRepository: RaumRepository,
                private val personRepository: PersonRepository) {

    fun frageAb(raumId: Raum.Id) : Result {

        val raum = raumRepository.lade(raumId) ?: return RaumExistiertNicht(raumId)

        // TODO [DDD] was ist ein BesetzterRaum? Warum nicht einfach Raum?
        // Ich habe mir die Klasse BesetzterRaum ausgedacht, als eine Klasse die einen Raum Objekt enthält und eine Liste
        // von Personen, ich wollte dieses Objekt nicht Raum benennen da ich es vom Ruam domain objekt object trennen wollte
        return BesetzterRaum(raum = raum, personen = personRepository.holePersonenMitIds(personenIds = raum.personen))
    }

    sealed class Result
    class RaumExistiertNicht(val raumId: Raum.Id) : Result()
    class BesetzterRaum(val raum: Raum, val personen: List<Person>) : Result()
}