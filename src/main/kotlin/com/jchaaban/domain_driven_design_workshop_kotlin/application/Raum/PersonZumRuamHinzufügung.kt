package com.jchaaban.domain_driven_design_workshop_kotlin.application.Raum

import com.jchaaban.domain_driven_design_workshop_kotlin.common.UseCase
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.RaumEventRepository
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.PersonWurdeRaumZugeordnetEvent
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.PersonRepository
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.RaumRepository

@UseCase
class PersonZumRuamHinzufügung(
    private val raumRepository: RaumRepository,
    private val personRepository: PersonRepository,
    private val personHinzugefügtEventRepository: RaumEventRepository,
) {
    fun fügePersonZumRaumHinzu(
        raumId: Raum.Id,
        personId: Person.Id,
    ): Result {
        val raum = raumRepository.lade(raumId) ?: return RaumExistiertNicht()

        if (!personRepository.existiert(personId)) {
            return PersonExistiertNicht()
        }

        try {
            raum.fügePersonHinzu(personId)
        } catch (exception : IllegalArgumentException){
            return PersonExistiertImRaum()
        }

        raumRepository.aktualisiere(raum)

        personHinzugefügtEventRepository.werfe(
            PersonWurdeRaumZugeordnetEvent(
                raumId,
                personId
            )
        )

        return PersonZumRaumHinzugefügt()
    }

    sealed class Result
    class PersonExistiertNicht : Result()
    class RaumExistiertNicht : Result()
    class PersonZumRaumHinzugefügt : Result()
    class PersonExistiertImRaum : Result()

}