package com.jchaaban.domain_driven_design_workshop_kotlin.application.Raum

import com.jchaaban.domain_driven_design_workshop_kotlin.common.UseCase
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.RaumRepository

@UseCase
class RaumAnlage(private val raumRepository: RaumRepository) {

    fun legeAn(
        raumname: String,
        raumnummer: String,
    ): Result {
        return when (val raumErzeugungErgebnis = erzeugeRaum(raumnummer = raumnummer, raumname = raumname)) {
            is Raum.InvalidArguments -> RaumInvalidArguments(raumErzeugungErgebnis.message)
            is Raum.Created -> speichereRaum(raum = raumErzeugungErgebnis.raum)
        }
    }

    // auf einem Raum.Result gibt es ein Speichern? Das Result ist doch schon ein Ergebnis?!
    // das geht schöner. was möchtest Du speichern? Den Raum! Dann kann man diesen auch
    // um einer Speichern()-Methode erweitern
//    private fun Raum.Result.speichereRaum(): Result = when (this) {
//        is Raum.Created -> this.legeAn()
//        is Raum.InvalidArguments -> RaumInvalidArguments(this.message)
//    }

    private fun erzeugeRaum(raumnummer: String, raumname: String): Raum.Result =
        Raum(raumnummer = raumnummer, raumname = raumname)

    private fun speichereRaum(raum: Raum): Result {
        val raumnummer = raum.raumnummer
        if (raumRepository existiert raumnummer) {
            return RaumExistiert(raumnummer)
        }
        raumRepository.legeAn(raum)
        return RaumAngelegt(raum)
    }

    sealed class Result
    class RaumAngelegt(val raum: Raum) : Result()
    class RaumExistiert(val raumnummer: Raum.Raumnummer) : Result()
    class RaumInvalidArguments(val message: String) : Result()

}



