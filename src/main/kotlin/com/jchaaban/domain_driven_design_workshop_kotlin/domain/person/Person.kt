package com.jchaaban.domain_driven_design_workshop_kotlin.domain.person

import com.jchaaban.domain_driven_design_workshop_kotlin.common.AggregateRoot
import com.jchaaban.domain_driven_design_workshop_kotlin.common.ValueObject
import java.util.*

@AggregateRoot
class Person private constructor(
    val id: Id,
    val vorname: Vorname,
    val nachname: Nachname,
    val namenszusatz: Namenszusatz?,
    val ldapBenutzername: LdapBenutzername,
) {
    @ValueObject
    data class Id(val value: UUID)

    @ValueObject
    data class Vorname(private val vorname: String) {
        val value: String = vorname.removeLeadingAndTrailingWhitespaces()

        init {
            require(value.isNotBlank()) { "Person.Vorname darf nicht leer sein" }
        }
    }

    @ValueObject
    data class Nachname(private val nachname: String) {
        val value: String = nachname.removeLeadingAndTrailingWhitespaces()

        init {
            require(value.isNotBlank()) { "Person.Nachname darf nicht leer sein" }
        }
    }

    @ValueObject
    enum class Namenszusatz {
        Von, Van, De;
        companion object {
            fun from(value: String): Namenszusatz {
                val zutreffendesEnum = entries.firstOrNull { it.name.lowercase() == value }
                return zutreffendesEnum ?: throw IllegalArgumentException("Der Namenszusatz muss von, van oder de sein")
            }
        }
    }


    @ValueObject
    data class LdapBenutzername(private val ldapBenutzername: String) {
        val value: String = ldapBenutzername.removeLeadingAndTrailingWhitespaces()

        init {
            require(value.isNotBlank()) { "Person.LdapBenutzername darf nicht leer sein" }
        }
    }

    companion object {
        operator fun invoke(
            vorname: String,
            nachname: String,
            namenzusatz: String? = null,
            ldapBenutzername: String,
        ): Result = try {
            Created(
                Person(
                    id = Id(value = UUID.randomUUID()),
                    vorname = Vorname(vorname = vorname),
                    nachname = Nachname(nachname = nachname),
                    namenszusatz = namenzusatz?.let { Namenszusatz.from(it) },
                    ldapBenutzername = LdapBenutzername(ldapBenutzername = ldapBenutzername)
                )
            )
        } catch (exception: IllegalArgumentException) {
            InvalidArguments(exception.message)
        }
    }

    fun kurzeSchreibweise(): String {
        val ldapUsernameRepresentation = "(${ldapBenutzername.value})"

        val personInfos = listOf(
            vorname.value,
            namenszusatz?.name?.lowercase(),
            nachname.value,
            ldapUsernameRepresentation
        ).filter { !it.isNullOrEmpty() }

        return personInfos.joinToString(" ")
    }

    sealed class Result
    class Created(val person: Person) : Result()

    class InvalidArguments(m: String?) : Result() {
        val message: String = m.orEmpty()
    }

    override fun toString(): String {
        return vorname.value + " " + nachname.value + " " + id.value + " " + ldapBenutzername.value
    }

}

private fun String.removeLeadingAndTrailingWhitespaces() = trim { it == ' ' }
