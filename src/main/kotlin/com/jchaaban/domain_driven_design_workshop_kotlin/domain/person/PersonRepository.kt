package com.jchaaban.domain_driven_design_workshop_kotlin.domain.person

import com.jchaaban.domain_driven_design_workshop_kotlin.common.Repository

@Repository
interface PersonRepository {
    fun legeAn(person: Person)
    fun existiert(personLdapBenutzername: Person.LdapBenutzername) : Boolean
    fun existiert(personId: Person.Id) : Boolean
    fun holePersonenMitIds(personenIds: List<Person.Id>) : List<Person> // die Mothode bekommt jetzt eine Liste von ID's
}