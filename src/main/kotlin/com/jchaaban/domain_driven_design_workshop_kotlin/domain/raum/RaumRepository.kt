package com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum

import com.jchaaban.domain_driven_design_workshop_kotlin.common.Repository

@Repository
interface RaumRepository {
    fun legeAn(raum: Raum)
    fun lade(raumId: Raum.Id) : Raum?
    infix fun existiert(raumnummer: Raum.Raumnummer) : Boolean
    fun aktualisiere(raum: Raum) // aktualisiere hinzugefügt
}