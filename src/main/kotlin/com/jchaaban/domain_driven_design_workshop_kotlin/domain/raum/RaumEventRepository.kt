package com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum

import com.jchaaban.domain_driven_design_workshop_kotlin.common.Repository

@Repository
interface RaumEventRepository {
    fun werfe(event: PersonWurdeRaumZugeordnetEvent)
}