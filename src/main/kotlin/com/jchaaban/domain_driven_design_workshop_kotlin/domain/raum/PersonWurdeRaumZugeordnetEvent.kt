package com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum

import com.jchaaban.domain_driven_design_workshop_kotlin.common.DomainEvent
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person

@DomainEvent
class PersonWurdeRaumZugeordnetEvent (
    private val raumId: Raum.Id,
    private val personId: Person.Id
)