package com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum

import com.jchaaban.domain_driven_design_workshop_kotlin.common.AggregateRoot
import com.jchaaban.domain_driven_design_workshop_kotlin.common.ValueObject
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import java.util.*

@AggregateRoot
class Raum private constructor(
    val id: Id,
    val raumnummer: Raumnummer,
    val raumname: Raumname,
    private val personIds : MutableList<Person.Id>
) {

    val personen : MutableList<Person.Id>
        get() = this.personIds

    @ValueObject
    data class Id(val value: UUID)

    @ValueObject
    data class Raumnummer(private val raumnummer: String){
        val value: String = raumnummer
        init {
            require(value.isNotBlank()) { "Raum.Raumnummer darf nicht leer sein" }
            require(value.length == 4) { "Raum.Raumnummer soll vierstellig sein" }
            require(value.all { it.isDigit() }) { "Raum.Raumnummer soll nur aus Zahlen bestehen" }
        }
    }

    @ValueObject
    data class Raumname(private val raumname: String){
        val value: String = raumname.removeLeadingAndTrailingWhitespaces()
        init {
            require(value.isNotBlank()) { "Raum.Raumname darf nicht leer sein" }
        }


    }

    companion object {
        operator fun invoke(
            raumnummer: String,
            raumname: String,
            personen: MutableList<Person.Id> = mutableListOf()
        ): Result = try {
            Created(
                Raum(
                    id = Id(value = UUID.randomUUID()),
                    raumnummer = Raumnummer(raumnummer = raumnummer),
                    raumname = Raumname(raumname = raumname),
                    personIds = personen
                )
            )
        } catch (exception: IllegalArgumentException) {
            InvalidArguments(exception.message)
        }
    }

    fun fügePersonHinzu(personId: Person.Id) {
        if (personIds enthält personId){
            throw IllegalArgumentException("Die Person die Sie zum Raum hinzufügen versuchen, existiert schon im Raum")
        }
        personIds.add(personId)
    }
    private infix fun List<Person.Id>.enthält(personenId: Person.Id) = this.contains(personenId)

    sealed class Result
    class Created(val raum: Raum) : Result()
    class InvalidArguments(m: String?) : Result() {
        val message: String = m.orEmpty()
    }
}
private fun String.removeLeadingAndTrailingWhitespaces() = trim { it == ' ' }
