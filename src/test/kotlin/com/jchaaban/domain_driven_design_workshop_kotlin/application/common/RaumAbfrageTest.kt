package com.jchaaban.domain_driven_design_workshop_kotlin.application.common

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.PersonRepository
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.erstellePersonMitTestDaten
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.erstelleRaumMitTestDaten
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.repository.RaumRepositoryImpl
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.UUID

internal class RaumAbfrageTest {

    private val personRepositoryMock = mockk<PersonRepository>()
    private val raumRepositoryMock = mockk<RaumRepositoryImpl>()
    private val raumAbfrage = RaumAbfrage(raumRepository = raumRepositoryMock, personRepository = personRepositoryMock)
    @Test
    internal fun `erhält einen vorhandenen leeren Raum`(){
        val raum = erstelleRaumMitTestDaten()
        val leerePersonenListe = emptyList<Person>()

        every { raumRepositoryMock.lade(raum.id) } returns raum
        every { personRepositoryMock.holePersonenMitIds(emptyList()) } returns leerePersonenListe

        val ergebnis = raumAbfrage.frageAb(raum.id)

        assertThat(ergebnis)
            .isInstanceOf(RaumAbfrage.BesetzterRaum::class.java)
            .extracting("raum").isEqualTo(raum)
            .extracting("personen").isEqualTo(leerePersonenListe)
    }

    @Test
    internal fun `erhält einen vorhandenen Raum mit Personen darin`(){
        val raum = erstelleRaumMitTestDaten()
        val erstePerson = erstellePersonMitTestDaten()
        val zweitePerson = erstellePersonMitTestDaten("Kai", "Renken")
        val personenListe = mutableListOf(erstePerson, zweitePerson)
        val personenIdListe = listOf(erstePerson.id, zweitePerson.id)

        personenListe.forEach {
            raum.fügePersonHinzu(personId = it.id)
        }

        every { raumRepositoryMock.lade(raum.id) } returns raum
        every { personRepositoryMock.holePersonenMitIds(personenIdListe) } returns personenListe
        val ergebnis = raumAbfrage.frageAb(raum.id)

        assertThat(ergebnis)
            .isInstanceOf(RaumAbfrage.BesetzterRaum::class.java)
            .extracting("raum").isEqualTo(raum)
            .extracting("personen").isEqualTo(personenIdListe)
    }

    @Test
    fun `erhält einen nicht vorhandenen Raum`(){
        val raumId = Raum.Id(UUID.randomUUID())
        every { raumRepositoryMock.lade(raumId = raumId) } returns null

        val ergebnis = raumAbfrage.frageAb(raumId = raumId)

        assertThat(ergebnis).isInstanceOf(RaumAbfrage.RaumExistiertNicht::class.java)
            .extracting("raumId").isEqualTo(raumId)

        verify(exactly = 0) {
            personRepositoryMock.holePersonenMitIds(personenIds = any())
        }
    }
}