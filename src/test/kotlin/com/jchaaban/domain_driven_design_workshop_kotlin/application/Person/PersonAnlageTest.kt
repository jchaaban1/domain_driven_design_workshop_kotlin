package com.jchaaban.domain_driven_design_workshop_kotlin.application.Person

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.PersonRepository
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.erstellePersonMitTestDaten
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class PersonAnlageTest {

    private val personRepository = mockk<PersonRepository>(relaxed = true)
    private val personAnlage = PersonAnlage(personRepository)

    @Test
    internal fun `eine nicht vorhandene Person hinzufügen`(){
        val person = erstellePersonMitTestDaten()
        every { personRepository.existiert(personLdapBenutzername = person.ldapBenutzername) } returns false

        val ergebnis = personAnlage.legeAn(
            person.vorname.value,
            person.nachname.value,
            person.namenszusatz?.name?.lowercase(),
            person.ldapBenutzername.value
        ) as PersonAnlage.PersonAngelegt

        assertThat(ergebnis.person.vorname.value).isEqualTo(person.vorname.value)
        assertThat(ergebnis.person.nachname.value).isEqualTo(person.nachname.value)
        assertThat(ergebnis.person.namenszusatz?.name?.lowercase()).isEqualTo(person.namenszusatz?.name?.lowercase())
        assertThat(ergebnis.person.ldapBenutzername.value).isEqualTo(person.ldapBenutzername.value)

        verify {
            personRepository.legeAn(person = withArg {
                assertThat(it.vorname.value).isEqualTo(person.vorname.value)
                assertThat(it.nachname.value).isEqualTo(person.nachname.value)
                assertThat(it.namenszusatz?.name?.lowercase()).isEqualTo(person.namenszusatz?.name?.lowercase())
                assertThat(it.ldapBenutzername.value).isEqualTo(person.ldapBenutzername.value)
            })
        }
    }

    @Test
    internal fun `eine vorhandene Person hinzufügen`(){
        val person = erstellePersonMitTestDaten()

        every { personRepository.existiert(personLdapBenutzername = person.ldapBenutzername) } returns true

        val ergebnis = personAnlage.legeAn(
            person.vorname.value,
            person.nachname.value,
            person.namenszusatz?.name?.lowercase(),
            person.ldapBenutzername.value
        )

        assertThat(ergebnis).isInstanceOf(PersonAnlage.PersonExistiert::class.java)

        verify(exactly = 0) { personRepository.legeAn(any()) }
    }

    @Test
    fun `eine ungültige Person hinzufügen`(){
        val ergebnis = personAnlage.legeAn(
            vorname = "Jaafar",
            nachname = "Chaaban",
            namenzusatz =  "dee",
            ldapBenutzername =  "jchaaban"
        )

        assertThat(ergebnis).isInstanceOf(PersonAnlage.PersonInvalidArguments::class.java)
        verify(exactly = 0) { personRepository.existiert(personLdapBenutzername = any()) }
        verify(exactly = 0) { personRepository.legeAn(person = any()) }
    }


}