package com.jchaaban.domain_driven_design_workshop_kotlin.application.Raum

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.erstelleRaumMitTestDaten
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Person.repository.PersonRepositoryImpl
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.repository.PersonHinzugefügtEventRepositoryImpl
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.repository.RaumRepositoryImpl
import io.mockk.*
import org.junit.jupiter.api.Test
import java.util.*
import org.assertj.core.api.Assertions.assertThat

internal class PersonZumRuamHinzufügungTest {

    private val raumRepositoryMock = mockk<RaumRepositoryImpl>()
    private val personRepositoryMock = mockk<PersonRepositoryImpl>()
    private val personHinzugefügtEventRepositoryMock = mockk<PersonHinzugefügtEventRepositoryImpl>()

    private val personZumRuamHinzufügung = PersonZumRuamHinzufügung(
        raumRepository = raumRepositoryMock,
        personRepository = personRepositoryMock,
        personHinzugefügtEventRepository = personHinzugefügtEventRepositoryMock
    )

    @Test
    internal fun `eine vorhandene Person zu einem vorhandenen Raum hinzufügen`(){
        val raum = erstelleRaumMitTestDaten()
        val personId = Person.Id(UUID.randomUUID())


        every { raumRepositoryMock.lade(raumId = raum.id) } returns raum
        every { personRepositoryMock.existiert(personId = personId) } returns true
        every { raumRepositoryMock.aktualisiere(any()) } just Runs
        every { personHinzugefügtEventRepositoryMock.werfe(any()) } just Runs

        val ergebnis = personZumRuamHinzufügung.fügePersonZumRaumHinzu(raumId = raum.id, personId = personId)

        assertThat(ergebnis).isInstanceOf(PersonZumRuamHinzufügung.PersonZumRaumHinzugefügt::class.java)

        verify (exactly = 1){
            raumRepositoryMock.aktualisiere(
                raum = withArg {
                     assertThat(it.raumnummer).isEqualTo(raum.raumnummer)
                     assertThat(it.raumname).isEqualTo(raum.raumname)
                }
            )
        }
    }

    @Test
    internal fun `eine vorhandene Person zu einem nicht vorhandenen Raum hinzufügen`(){
        val raumId = Raum.Id(UUID.randomUUID())
        val personId = Person.Id(UUID.randomUUID())

        every { raumRepositoryMock.lade(raumId = raumId) } returns null

        val result = personZumRuamHinzufügung.fügePersonZumRaumHinzu(raumId = raumId, personId = personId)

        assertThat(result).isInstanceOf(PersonZumRuamHinzufügung.RaumExistiertNicht::class.java)

        verify (exactly = 0){ personRepositoryMock.existiert(personId = personId)}
        verify (exactly = 0){ raumRepositoryMock.aktualisiere(raum = any())}
        verify (exactly = 0){ personHinzugefügtEventRepositoryMock.werfe(event = any())}

    }

    @Test
    internal fun `eine nicht vorhandene Person zu einem vorhandenen Raum hinzufügen`(){
        val raum = erstelleRaumMitTestDaten()
        val personId = Person.Id(UUID.randomUUID())

        every { raumRepositoryMock.lade(raumId = raum.id) } returns raum
        every { personRepositoryMock.existiert(personId = personId) } returns false

        val ergebnis = personZumRuamHinzufügung.fügePersonZumRaumHinzu(raumId = raum.id, personId = personId)

        assertThat(ergebnis).isInstanceOf(PersonZumRuamHinzufügung.PersonExistiertNicht::class.java)

        verify (exactly = 0){ raumRepositoryMock.aktualisiere(any())}
        verify (exactly = 0){ personHinzugefügtEventRepositoryMock.werfe(any())}
    }

    @Test
    internal fun `eine Person zu einem Raum hinzufügen, in dem sie bereits existiert`(){
        val raum = erstelleRaumMitTestDaten()
        val personId = Person.Id(UUID.randomUUID())
        raum.fügePersonHinzu(personId)

        every { raumRepositoryMock.lade(raumId = raum.id) } returns raum
        every { personRepositoryMock.existiert(personId = personId) } returns true


        val result = personZumRuamHinzufügung.fügePersonZumRaumHinzu(raumId = raum.id, personId = personId)

        assertThat(result).isInstanceOf(PersonZumRuamHinzufügung.PersonExistiertImRaum::class.java)

        verify (exactly = 0){ raumRepositoryMock.aktualisiere(any())}
        verify (exactly = 0){ personHinzugefügtEventRepositoryMock.werfe(any())}
    }
}