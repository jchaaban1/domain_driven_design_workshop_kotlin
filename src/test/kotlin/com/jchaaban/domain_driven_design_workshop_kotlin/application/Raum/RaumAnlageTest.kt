package com.jchaaban.domain_driven_design_workshop_kotlin.application.Raum

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.RaumRepository
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.erstelleRaumMitTestDaten
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test

internal class RaumAnlageTest {

   private val raumRepositoryMock = mockk<RaumRepository>(relaxed = true)
   private val raumAnlage = RaumAnlage(raumRepositoryMock)

   @Test
   fun `einen nicht gültigen vorhandenen Raum hinzufügen`(){
      val raum = erstelleRaumMitTestDaten()
      every { raumRepositoryMock.existiert(raum.raumnummer) } returns false

      val ergebnis = raumAnlage.legeAn(raum.raumname.value, raum.raumnummer.value) as RaumAnlage.RaumAngelegt

      assertThat(raum.raumnummer.value).isEqualTo(ergebnis.raum.raumnummer.value)
      assertThat(raum.raumname.value).isEqualTo(ergebnis.raum.raumname.value)

      verify {
         raumRepositoryMock.legeAn(withArg {
            assertThat(it.raumname.value).isEqualTo(raum.raumname.value)
            assertThat(it.raumnummer.value).isEqualTo(raum.raumnummer.value)
         })
      }
   }

   @Test
   fun `einen vorhandenen Raum hinzufügen`(){
      val raum = erstelleRaumMitTestDaten()

      every { raumRepositoryMock.existiert(raum.raumnummer) } returns true

      val ergebnis = raumAnlage.legeAn(raumnummer = raum.raumnummer.value, raumname = raum.raumname.value)

      assertThat(ergebnis).isInstanceOf(RaumAnlage.RaumExistiert::class.java)

      verify(exactly = 0){ raumRepositoryMock.legeAn(raum) }
   }

   @Test
   fun `einen ungültigen Raum hinzufügen`(){
      val raumnummer = "111a"
      val raumname = "Raum 1"

      val ergebnis = raumAnlage.legeAn(raumnummer = raumnummer, raumname = raumname)

      assertThat(ergebnis).isInstanceOf(RaumAnlage.RaumInvalidArguments::class.java)
      verify (exactly = 0){ raumRepositoryMock.existiert(any()) }
      verify (exactly = 0){ raumRepositoryMock.legeAn(any()) }
   }

}