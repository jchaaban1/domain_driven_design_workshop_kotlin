package com.jchaaban.domain_driven_design_workshop_kotlin

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.erstellePersonMitTestDaten
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.erstelleRaumMitTestDaten
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Person.rest.toJson
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.repository.PersonHinzugefügtEventRepositoryImpl
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto.PersonAbfrageDto
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.dto.RaumAbfrageDto
import com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest.toJson
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import org.assertj.core.api.Assertions.assertThat
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class IntegrationTestIT {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Autowired
    private lateinit var personHinzugefügtEventRepository: PersonHinzugefügtEventRepositoryImpl

    @Test
    fun `erstelle eine neue Person, erstell ein neuer Raum, füge die Person zum Raum hinzu und lade den Raum`(){
        val raum = erstelleRaumMitTestDaten()
        val person = erstellePersonMitTestDaten()

        val roomId = mockMvc.perform(
            post("/api/room")
                .contentType(MediaType.APPLICATION_JSON)
                .content(raum.toJson())
        ).andExpect(status().isOk)
            .andExpect(jsonPath("$.raumnummer").value(raum.raumnummer.value))
            .andExpect(jsonPath("$.raumname").value(raum.raumname.value))
            .andDo(print())
            .andReturn().response.contentAsString.mapToReadRoomDto().mapToRoomId()

        val personId = mockMvc.perform(
            post("/api/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(person.toJson())
        ).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.vorname").value(person.vorname.value))
            .andExpect(jsonPath("$.nachname").value(person.nachname.value))
            .andExpect(jsonPath("$.namenszusatz").value(person.namenszusatz!!.name.lowercase()))
            .andExpect(jsonPath("$.ldapBenutzername").value(person.ldapBenutzername.value))
            .andReturn().response.contentAsString.mapToReadPersonDto().mapToPersonId()

        mockMvc.perform(
            put("/api/room/$roomId/person/$personId")
        ).andDo (print())
            .andExpect(status().isOk)
        mockMvc.perform(
            get("/api/room/$roomId")
        ).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id").value(roomId))
            .andExpect(jsonPath("$.raumnummer").value(raum.raumnummer.value))
            .andExpect(jsonPath("$.raumname").value(raum.raumname.value))
            .andExpect(jsonPath("$.personen").isArray)
            .andExpect(jsonPath("$.personen.length()").value(1))
            .andExpect(jsonPath("$.personen[0]").value(person.kurzeSchreibweise()))

        assertThat(personHinzugefügtEventRepository.size()).isEqualTo(1)
    }

}

private fun RaumAbfrageDto.mapToRoomId() = this.id
private fun String.mapToReadRoomDto(): RaumAbfrageDto = jacksonObjectMapper().readValue(this, RaumAbfrageDto::class.java)
private fun PersonAbfrageDto.mapToPersonId() = this.id
private fun String.mapToReadPersonDto() = jacksonObjectMapper().readValue(this, PersonAbfrageDto::class.java)

