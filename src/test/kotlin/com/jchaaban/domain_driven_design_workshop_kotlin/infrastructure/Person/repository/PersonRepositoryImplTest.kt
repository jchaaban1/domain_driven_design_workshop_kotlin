package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Person.repository

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.erstellePersonMitTestDaten
import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.assertThat
internal class PersonRepositoryImplTest {

    private val personRepositoryImpl = PersonRepositoryImpl()

    @Test
    internal fun `lege eine Person an`(){
        val person = erstellePersonMitTestDaten()
        personRepositoryImpl.legeAn(person)
        assertThat(personRepositoryImpl.size()).isEqualTo(1)
    }

    @Test
    internal fun `enthält eine Person mit id`(){
        val person = erstellePersonMitTestDaten()
        personRepositoryImpl.legeAn(person)

        assertThat(personRepositoryImpl.existiert(personId = person.id)).isTrue()
    }

    @Test
    internal fun `enthält eine Person mit ldapBenutzername`(){
        val person = erstellePersonMitTestDaten()
        personRepositoryImpl.legeAn(person)

        assertThat(personRepositoryImpl.existiert(personLdapBenutzername = person.ldapBenutzername)).isTrue()
    }

    @Test
    fun `lade Personen mit ids`(){
        val erstePerson = erstellePersonMitTestDaten()
        val zweitePerson = erstellePersonMitTestDaten()

        personRepositoryImpl.legeAn(erstePerson)
        personRepositoryImpl.legeAn(zweitePerson)

        val personen = personRepositoryImpl.holePersonenMitIds(listOf(erstePerson.id, zweitePerson.id))

        assertThat(personRepositoryImpl.size()).isEqualTo(2)
        assertThat(personen[0]).isEqualTo(erstePerson)
        assertThat(personen[1]).isEqualTo(zweitePerson)
    }


}