package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Person.rest

import com.jchaaban.domain_driven_design_workshop_kotlin.application.Person.PersonAnlage
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.erstellePersonMitTestDaten
import com.ninjasquad.springmockk.MockkBean
import io.mockk.CapturingSlot
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status

@WebMvcTest(PersonController::class)
internal class PersonControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var personAnlage: PersonAnlage

    @Test
    internal fun `post gültige Person`(){
        val person = erstellePersonMitTestDaten()

        every { personAnlage.legeAn(any(), any(), any(), any()) } returns PersonAnlage.PersonAngelegt(person)

        mockMvc.perform(
            post("/api/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(person.toJson())
        ).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.vorname").value(person.vorname.value))
            .andExpect(jsonPath("$.nachname").value(person.nachname.value))
            .andExpect(jsonPath("$.ldapBenutzername").value(person.ldapBenutzername.value))
            .andExpect(jsonPath("$.namenszusatz").value(person.namenszusatz?.name?.lowercase()))
            .andReturn()
            .response.contentAsString

        val vornameSlot : CapturingSlot<String> = slot()
        val nachnameSlot : CapturingSlot<String> = slot()
        val namenszusatzSlot : CapturingSlot<String> = slot()
        val ldapBenutzernameSlot : CapturingSlot<String> = slot()

        verify {
            personAnlage.legeAn(
                vorname = capture(vornameSlot),
                nachname = capture(nachnameSlot),
                namenzusatz = capture(namenszusatzSlot),
                ldapBenutzername = capture(ldapBenutzernameSlot)
            )
        }

        assertThat(vornameSlot.captured).isEqualTo(person.vorname.value)
        assertThat(nachnameSlot.captured).isEqualTo(person.nachname.value)
        assertThat(namenszusatzSlot.captured.lowercase()).isEqualTo(person.namenszusatz?.name?.lowercase())
        assertThat(ldapBenutzernameSlot.captured).isEqualTo(person.ldapBenutzername.value)
    }

    @Test
    internal fun `füge eine ungültige Person hinzu`(){

        val vorname = "Jaafar"
        val nachname = "Chaaban"
        val namenzusatz =  "dee"
        val ldapBenutzername = "jchaaban"

        every { personAnlage.legeAn(any(), any(), any(), any()) } returns PersonAnlage.PersonInvalidArguments(
            "Der Namenzusatz muss von, van oder de sein"
        )


        val response = mockMvc.perform(
            post("/api/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content("""
                    {
                      "vorname": "$vorname",
                      "nachname": "$nachname",
                      "ldapBenutzername" : "$ldapBenutzername",
                      "namenszusatz": "$namenzusatz"
                    }
                """.trimIndent())
        ).andDo(print())
            .andExpect(status().is4xxClientError)
            .andReturn().response.contentAsString

        assertThat(response).isEqualTo("Der Namenzusatz muss von, van oder de sein")
    }

    @Test
    internal fun `post eine bereits existierte Person`(){
        val person = erstellePersonMitTestDaten()

        every { personAnlage.legeAn(
            vorname = any(),
            nachname =  any(),
            namenzusatz =  any(),
            ldapBenutzername =  any())
        } returns PersonAnlage.PersonExistiert(ldapBenutzername = person.ldapBenutzername)

        val result = mockMvc.perform(
            post("/api/person")
                .contentType(MediaType.APPLICATION_JSON)
                .content(person.toJson())
        ).andDo(print())
            .andExpect(status().is4xxClientError)
            .andReturn()
            .response
            .contentAsString

        assertThat(result).isEqualTo("Der Person mit dem ldapBenutzername: ${person.ldapBenutzername.value} existiert schon")
    }
}



fun Person.toJson() = """
    {
        "vorname": "${this.vorname.value}",
        "nachname": "${this.nachname.value}",
        "ldapBenutzername": "${this.ldapBenutzername.value}",
        "namenszusatz": "${this.namenszusatz?.name?.lowercase().orEmpty()}"
    }
""".trimIndent()
