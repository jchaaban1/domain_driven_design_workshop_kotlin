package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.repository

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.erstelleRaumMitTestDaten
import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.assertThat

internal class RaumRepositoryImplTest {

    private val raumRepository = RaumRepositoryImpl()
    @Test
    fun `lege einen Raum an`(){
        val raum = erstelleRaumMitTestDaten()

        raumRepository.legeAn(raum)

        assertThat(raumRepository.size).isEqualTo(1)
    }

    @Test
    fun `lade ein gespeicherter Raum`(){
        val raum = erstelleRaumMitTestDaten()

        raumRepository.legeAn(raum)

        val geladenerRaum = raumRepository.lade(raum.id)

        assertThat(geladenerRaum).isEqualTo(raum)
    }
    @Test
    fun `aktualisier ein gespeicherter Raum`(){
        val raum = erstelleRaumMitTestDaten()

        raumRepository.legeAn(raum)
        raumRepository.aktualisiere(raum)

        val geladenerRaum = raumRepository.lade(raum.id)!!

        assertThat(raumRepository.size).isEqualTo(1)
        assertThat(geladenerRaum.id.value).isEqualTo(raum.id.value)
        assertThat(geladenerRaum.raumname.value).isEqualTo(raum.raumname.value)
        assertThat(geladenerRaum.raumnummer.value).isEqualTo(raum.raumnummer.value)
    }

    @Test
    fun `einhält Raum`(){
        val raum = erstelleRaumMitTestDaten()

        raumRepository.legeAn(raum)

        assertThat(raumRepository.existiert(raum.raumnummer)).isTrue()
    }

    @Test
    fun `Raum existiert nicht`(){
        assertThat(raumRepository.existiert(Raum.Raumnummer("1111"))).isFalse()
    }
}