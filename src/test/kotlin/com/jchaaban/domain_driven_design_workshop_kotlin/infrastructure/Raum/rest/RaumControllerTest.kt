package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.rest

import com.jchaaban.domain_driven_design_workshop_kotlin.application.Raum.PersonZumRuamHinzufügung
import com.jchaaban.domain_driven_design_workshop_kotlin.application.Raum.RaumAnlage
import com.jchaaban.domain_driven_design_workshop_kotlin.application.common.RaumAbfrage
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.erstellePersonMitTestDaten
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.erstelleRaumMitTestDaten
import com.ninjasquad.springmockk.MockkBean
import io.mockk.CapturingSlot
import io.mockk.every
import io.mockk.slot
import io.mockk.verify
import org.junit.jupiter.api.Test
import org.assertj.core.api.Assertions.assertThat
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.result.MockMvcResultHandlers.print
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.UUID

@WebMvcTest(RaumController::class)
internal class RaumControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockkBean
    private lateinit var raumAnlage: RaumAnlage

    @MockkBean
    private lateinit var raumAbfrage: RaumAbfrage

    @MockkBean
    private lateinit var personZumRuamHinzufügung: PersonZumRuamHinzufügung

    @Test
    internal fun `post ein gültiger Raum`(){

        val raum = erstelleRaumMitTestDaten()

        every { raumAnlage.legeAn(any(), any()) } returns RaumAnlage.RaumAngelegt(raum = raum)

        mockMvc.perform(
            post("/api/room")
                .contentType(MediaType.APPLICATION_JSON)
                .content(raum.toJson())
        ).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.id").value(raum.id.value.toString()))
            .andExpect(jsonPath("$.raumnummer").value(raum.raumnummer.value))
            .andExpect(jsonPath("$.raumname").value(raum.raumname.value))
            .andExpect(jsonPath("$.personen").isArray)
            .andExpect(jsonPath("$.personen").isEmpty)
            .andReturn().response.contentAsString

        val raumnameSlot: CapturingSlot<String> = slot()
        val raumnummerSlot: CapturingSlot<String> = slot()

        verify {
            raumAnlage.legeAn(
                raumnummer = capture(raumnummerSlot),
                raumname = capture(raumnameSlot)
            )
        }

        assertThat(raumnameSlot.captured).isEqualTo(raum.raumname.value)
        assertThat(raumnummerSlot.captured).isEqualTo(raum.raumnummer.value)
    }

    @Test
    internal fun `post ein Raum mit ungültigem Raumnummer`(){

        every { raumAnlage.legeAn(any(), any()) } returns
                RaumAnlage.RaumInvalidArguments("Raum.Raumnummer soll nur aus Zahlen bestehen")


        val response = mockMvc.perform(
            post("/api/room")
                .contentType(MediaType.APPLICATION_JSON)
                .content(
                    """
                        {
                            "raumname": "raum 1",
                            "raumnummer" : "111f" 
                        }
                    """.trimIndent()
                )
        ).andDo(print())
            .andExpect(status().is4xxClientError)
            .andReturn().response.contentAsString

        assertThat(response).isEqualTo("Raum.Raumnummer soll nur aus Zahlen bestehen")
    }

    @Test
    internal fun `post ein Raum, der bereits existiert`(){
        val raum = erstelleRaumMitTestDaten()
        every { raumAnlage.legeAn(any(), any()) } returns RaumAnlage.RaumExistiert(raum.raumnummer)

        val response = mockMvc.perform(
            post("/api/room")
                .contentType(MediaType.APPLICATION_JSON)
                .content(raum.toJson())
        ).andDo(print())
            .andExpect(status().is4xxClientError)
            .andReturn().response.contentAsString

        assertThat(response).isEqualTo("Der Raum mit dem Nummer: ${raum.raumnummer.value} existiert schon")
    }

    @Test
    internal fun `get ein leerer Raum, der bereits existiert`(){
        val raum = erstelleRaumMitTestDaten()
        every { raumAbfrage.frageAb(raumId = raum.id) } returns RaumAbfrage.BesetzterRaum(raum, emptyList())

        mockMvc.perform(
            get("/api/room/${raum.id.value}")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.raumnummer").value(raum.raumnummer.value))
            .andExpect(jsonPath("$.raumname").value(raum.raumname.value))
            .andExpect(jsonPath("$.personen").isArray)
            .andExpect(jsonPath("$.personen").isEmpty)
    }

    @Test
    internal fun `get ein Raum mit Personen drinne, der bereits existiert`(){
        val raum = erstelleRaumMitTestDaten()
        val person = erstellePersonMitTestDaten()

        every { raumAbfrage.frageAb(raum.id) } returns RaumAbfrage.BesetzterRaum(raum, listOf(person))

        mockMvc.perform(
            get("/api/room/${raum.id.value}")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
            .andExpect(status().isOk)
            .andExpect(jsonPath("$.raumname").value(raum.raumname.value))
            .andExpect(jsonPath("$.raumnummer").value(raum.raumnummer.value))
            .andExpect(jsonPath("$.personen").isArray)
            .andExpect(jsonPath("$.personen.length()").value(1))
            .andExpect(jsonPath("$.personen[0]").value(person.kurzeSchreibweise()))
    }

    @Test
    internal fun `get ein Raum, der nicht existiert`(){

        val raumId = Raum.Id(UUID.randomUUID())
        every { raumAbfrage.frageAb(raumId) } returns RaumAbfrage.RaumExistiertNicht(raumId)

        mockMvc.perform(
            get("/api/room/${raumId.value}")
                .contentType(MediaType.APPLICATION_JSON)
        ).andDo(print())
            .andExpect(status().isNotFound)

    }



}

fun Raum.toJson() = """
    {
        "raumname": "${this.raumname.value}",
        "raumnummer" : "${this.raumnummer.value}" 
    }
""".trimIndent()
