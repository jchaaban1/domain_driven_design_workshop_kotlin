package com.jchaaban.domain_driven_design_workshop_kotlin.infrastructure.Raum.repository

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.PersonWurdeRaumZugeordnetEvent
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum.Raum
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import java.util.UUID

internal class PersonHinzugefügtEventRepositoryImplTest {

    private val personHinzugefügtEventRepositoryImpl = PersonHinzugefügtEventRepositoryImpl()
    @Test
    internal fun `werfe ein neues Event`(){
        val event = PersonWurdeRaumZugeordnetEvent(
            raumId = Raum.Id(UUID.randomUUID()),
            personId = Person.Id(UUID.randomUUID())
        )

        personHinzugefügtEventRepositoryImpl.werfe(event = event)

        assertThat(personHinzugefügtEventRepositoryImpl.size()).isEqualTo(1)
    }
}