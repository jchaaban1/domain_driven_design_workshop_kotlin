package com.jchaaban.domain_driven_design_workshop_kotlin.domain.person

fun erstellePersonMitTestDaten(
    vorname: String = "Jaafar",
    nachname: String = "Chaaban",
    namenszusatz: String? = "von",
    ldapBenutzername: String = "jchaaban",
): Person {
    val  person = Person(vorname, nachname, namenszusatz, ldapBenutzername) as Person.Created
    return person.person
}

/*
    fun createPersonWithTestData(
        vorname: String = "Jaafar",
        nachname: String = "Chaaban",
        namenszusatz: String? = "von",
        ldapBenutzername: String = "jchaaban",
    ): Person = when (val result = Person(vorname, nachname, namenszusatz, ldapBenutzername)) {
        is Person.Created -> result.person
        is Person.InvalidArguments -> throw IllegalArgumentException(result.message)
        else -> throw Exception() // ?
    }
 */

