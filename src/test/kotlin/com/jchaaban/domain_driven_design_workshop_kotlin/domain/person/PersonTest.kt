package com.jchaaban.domain_driven_design_workshop_kotlin.domain.person

import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.EnumSource
import org.junit.jupiter.params.provider.ValueSource

internal class PersonTest {
    @Nested
    @DisplayName("Erstellen von Person.Vorname")
    inner class ErstelleVorname {
        @ParameterizedTest
        @ValueSource(strings = ["Jaafar", "Tom", "Kai"])
        internal fun `ist gültig`(vorname: String){
            assertThat(Person.Vorname(vorname).value).isEqualTo(vorname)
        }

        @ParameterizedTest
        @ValueSource(strings = [" Jaafar", "Jaafar ", " Jaafar "])
        internal fun `hat leere führende oder nachfolgende Zeichen`(vorname: String){
            assertThat(Person.Vorname(vorname).value).isEqualTo("Jaafar")
        }

        @ParameterizedTest
        @ValueSource(strings = ["", " ", "   "])
        internal fun `ist leer oder besteht aus Leerzeichen`(vorname: String){
            Assertions.assertThatThrownBy{ Person.Vorname(vorname) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Person.Vorname darf nicht leer sein")
        }
    }

    @Nested
    @DisplayName("Erstellen von Person.Nachname")
    inner class ErstelleNachname {
        @ParameterizedTest
        @ValueSource(strings = ["Chaaban", "Müller", "Renken"])
        internal fun `ist gültig`(nachname: String){
            assertThat(Person.Nachname(nachname).value).isEqualTo(nachname)
        }

        @ParameterizedTest
        @ValueSource(strings = [" Chaaban", "Chaaban ", " Chaaban "])
        internal fun `hat leere führende oder nachfolgende Zeichen`(nachname: String){
            assertThat(Person.Nachname(nachname).value).isEqualTo("Chaaban")
        }

        @ParameterizedTest
        @ValueSource(strings = ["", " ", "   "])
        internal fun `ist leer oder besteht aus Leerzeichen`(nachname: String){
            Assertions.assertThatThrownBy{ Person.Nachname(nachname) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Person.Nachname darf nicht leer sein")
        }
    }

    @Nested
    @DisplayName("Erstellen von Person.LdapBenutzername")
    inner class ErstelleLdapBenutzername {

        @ParameterizedTest
        @ValueSource(strings = ["jchaaban", "krenken", "tmüller"])
        internal fun `ist gültig`(ldapBenutzername: String){
            assertThat(Person.LdapBenutzername(ldapBenutzername).value).isEqualTo(ldapBenutzername)
        }

        @ParameterizedTest
        @ValueSource(strings = [" jchaaban", "jchaaban ", " jchaaban "])
        internal fun `hat leere führende oder nachfolgende Zeichen`(ldapBenutzername: String){
            assertThat(Person.LdapBenutzername(ldapBenutzername).value).isEqualTo("jchaaban")
        }

        @ParameterizedTest
        @ValueSource(strings = ["", " ", "   "])
        internal fun `ist leer oder besteht aus Leerzeichen`(ldapBenutzername: String){
            Assertions.assertThatThrownBy{ Person.LdapBenutzername(ldapBenutzername) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Person.LdapBenutzername darf nicht leer sein")
        }
    }

    @Nested
    @DisplayName("Erstellen von Person.Namenszusatz")
    inner class ErstelleNamenszusatz {
        @ParameterizedTest
        @EnumSource(Person.Namenszusatz::class)
        internal fun `ist gültig`(namenszusatz: Person.Namenszusatz){
            val person = erstellePersonMitTestDaten(namenszusatz = namenszusatz.name.lowercase())
            assertThat(person.namenszusatz).isEqualTo(namenszusatz)
        }

        @ParameterizedTest
        @ValueSource(strings = ["dee", "vonn", "vo?", "??"])
        internal fun `ist ungültig`(namenszusatz: String){
            Assertions.assertThatThrownBy { Person.Namenszusatz.from(namenszusatz) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Der Namenszusatz muss von, van oder de sein")
        }
    }
}