package com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person

fun erstelleRaumMitTestDaten(
    raumName: String = "Raum 1",
    raumnummer: String = "1111",
    personen: MutableList<Person.Id> = mutableListOf()
): Raum {
    val  raum = Raum(raumnummer, raumName, personen) as Raum.Created
    return raum.raum
}
