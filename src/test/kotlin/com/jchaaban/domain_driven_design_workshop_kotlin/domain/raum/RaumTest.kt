package com.jchaaban.domain_driven_design_workshop_kotlin.domain.raum

import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.Person
import com.jchaaban.domain_driven_design_workshop_kotlin.domain.person.erstellePersonMitTestDaten
import org.assertj.core.api.Assertions.assertThat
import org.assertj.core.api.Assertions.assertThatThrownBy
import org.junit.jupiter.api.DisplayName
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import java.lang.IllegalArgumentException
import java.util.*

internal class RaumTest {
    @Nested
    @DisplayName("Erstelle Raum.Nummer")
    inner class RaumNummerTest {
        @ParameterizedTest
        @ValueSource(strings = ["1111", "0000", "2343"])
        internal fun `läuft erfolgreich, hat 4 Ziffern`(nummer: String){
            assertThat(Raum.Raumnummer(nummer).value).isEqualTo(nummer)
        }

        @ParameterizedTest
        @ValueSource(strings = ["1", "22", "333"])
        internal fun `hat weniger als 4 Ziffern`(nummer: String){
            assertThatThrownBy { Raum.Raumnummer(nummer) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Raum.Raumnummer soll vierstellig sein")
        }

        @Test
        internal fun `hat mehr als 4 Ziffern`(){
            assertThatThrownBy { Raum.Raumnummer("11111") }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Raum.Raumnummer soll vierstellig sein")
        }

        @ParameterizedTest
        @ValueSource(strings = ["", " ", "  ", "    "])
        internal fun `ist leer oder besteht aus Leerzeichen`(nummer: String){
            assertThatThrownBy { Raum.Raumnummer("") }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Raum.Raumnummer darf nicht leer sein")
        }

        @ParameterizedTest
        @ValueSource(strings = [" 111", "111 ", " 11 "])
        internal fun `hat leere Zeichen`(nummer: String){
            assertThatThrownBy { Raum.Raumnummer(nummer) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Raum.Raumnummer soll nur aus Zahlen bestehen")
        }

        @ParameterizedTest
        @ValueSource(strings = ["s111", "1w11", "11s1", "111s", "ssss"])
        internal fun `enthält Buchstaben`(nummer: String){
            assertThatThrownBy { Raum.Raumnummer(nummer) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Raum.Raumnummer soll nur aus Zahlen bestehen")
        }
    }

    @Nested
    @DisplayName("Erstelle Raum.Name")
    inner class RaumNameTest {
        @ParameterizedTest
        @ValueSource(strings = ["R", "Raum 111"])
        internal fun `ist gültig, nicht leer oder besteht aus Leerzeichen`(name: String){
            assertThat(Raum.Raumname(name).value).isEqualTo(name)
        }

        @ParameterizedTest
        @ValueSource(strings = ["", " ", "   "])
        internal fun `ist leer oder besteht aus Leerzeichen`(name: String){
            assertThatThrownBy { Raum.Raumname(name) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Raum.Raumname darf nicht leer sein")
        }

        @ParameterizedTest
        @ValueSource(strings = [" Raum 1", "Raum 1 ", " Raum 1 "])
        internal fun `Wert ist nicht getrimmt`(name: String){
            assertThat(Raum.Raumname(name).value).isEqualTo("Raum 1")
        }
    }

    @DisplayName("Erstelle Raum mit ID")
    @Nested
    inner class RaumIdTest{
        @Test
        internal fun `wird automatisch generiert`(){
            val raumnummer = "1111"
            val raumname = "Raum 1"

            val raum : Raum = erstelleRaumMitTestDaten(raumName = raumname, raumnummer = raumnummer)

            assertThat(raum.id).isNotNull
            assertThat(raum.raumname.value).isEqualTo(raumname)
            assertThat(raum.raumnummer.value).isEqualTo(raumnummer)
        }
    }

    @DisplayName("Erstelle room mit")
    @Nested
    inner class RoomWithPersons {

        @Test
        internal fun `keine Personen`(){
            val raum = erstelleRaumMitTestDaten()
            assertThat(raum.personen).isEmpty()
        }

        @Test
        internal fun `zwei personen`(){
            val raum = erstelleRaumMitTestDaten()
            raum.fügePersonHinzu(Person.Id(UUID.randomUUID()))
            raum.fügePersonHinzu(Person.Id(UUID.randomUUID()))

            assertThat(raum.personen.size).isEqualTo(2)
        }

        @Test
        internal fun `doppelte person`(){
            val raum = erstelleRaumMitTestDaten()
            val personId = Person.Id(UUID.randomUUID())

            raum.fügePersonHinzu(personId)

            assertThatThrownBy { raum.fügePersonHinzu(personId) }
                .isInstanceOf(IllegalArgumentException::class.java)
                .hasMessage("Die Person die Sie zum Raum hinzufügen versuchen, existiert schon im Raum")
        }
    }

    @Nested
    @DisplayName("Test Kurzschreibweise mit")
    inner class KurzSchreibweiseTest{
        @Test
        fun `minimale Person`(){
            val person = erstellePersonMitTestDaten(namenszusatz = null)
            val ergebnis = person.kurzeSchreibweise()
            assertThat(ergebnis).isEqualTo("Jaafar Chaaban (jchaaban)")
        }

        @Test
        fun `Person mit Namenszusatz`(){
            val person = erstellePersonMitTestDaten()
            val ergebnis = person.kurzeSchreibweise()
            assertThat(ergebnis).isEqualTo("Jaafar von Chaaban (jchaaban)")
        }
    }
}