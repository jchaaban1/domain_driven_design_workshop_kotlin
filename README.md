Domain Driven Design Workshop with Kotlin
==========================================

This project is my implementation of the workshop on Domain Driven Design (DDD) and Hexagonal Architecture. 
The workshop can be found [here](https://larmic.de/workshops/ddd_hexagonal_architecture_for_devs/).

Requirements
------------

- Java 17
- Kotlin 1.9.0
- Docker

Build and Run
-------------

To build and run the project, use the following commands:

```shell
$ ./mvnw clean package
$ ./mvnw spring-boot:run
```

Project Structure
-----------------

The project is structured following the principles of Domain Driven Design and Hexagonal Architecture. Below is a brief overview of the key components and their locations:

- **Domain Layer**: Contains the core business logic and domain aggregates/entites.
- **Infrastructure Layer**: Contains the implementation of repositories and REST controllers.
- **Application Layer**: Contains the application use cases.

Tests
-----

The project includes various unit and integration test classes to ensure the functionality of all application components

Running integration and unit tests:

```shell
$ ./mvnw clean verify
```

## Usage

To interact with the APIs and perform operations such as creating a new person, creating a new room, adding the person to the room, and retrieving room details, follow these steps:

1. **Create a new room**:

```http
POST /api/room
Content-Type: application/json

{
 "raumname": "Raum 1",
 "raumnummer": "1011"
}
```

2. **Create a new person**:

```http
POST /api/person
Content-Type: application/json

{
"vorname": "Max",
"nachname": "Mustermann",
"ldapBenutzername": "mmustermann",
"namenszusatz": "von"
}
```

3. **Add the person to the room**:
```http
PUT /api/room/{roomId}/person/{personId}
```

4. **Get the room details**:
```http
GET /api/room/{roomId}
```